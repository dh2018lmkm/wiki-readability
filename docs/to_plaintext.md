# Instructions for `to_plaintext.py`

* Navigate to its directory.
* Execute using: `python3 to_plaintext.py file1.json file2.json file3.json`

## Required 

* python3 *(programmed and tested using Python 3.6.7)*.
* the `sys`, `os`, and `re` libraries *(download using pip3)*.
* the `.json` file(s) that you want to convert.

## Further notes

* Tested on Ubuntu 18.04 *(compatibility for Windows and MacOS not guaranteed)*.
* Does not produce output on terminal except for a few error messages.
* Large files *(> 25 MiB)* can take several seconds to convert.
* Huge files *(> 1 GiB)* can take seveal hours to convert.
* Each `.json` file is loaded into RAM entirely.
* Contact kf47xype@studserv.uni-leipzig.de in case of grave errors.
