# config.json


## general

**`sleep`**:  
Time in seconds to wait before an API call

**`results_dir`**:  
Directory for results.

**`results_file`**:  
JSON file within the results directory that results are written to.

**`results_graphs_dir`**:  
Graphs directory within the results directory.

**`cache_file`**:  
Location of the cache file (used for some api calls).


## corpus

**`wiki_url`**:  
URL to a Mediawiki instance.

**`corpus_list_file`**:  
Path of a json file containing a list of articles.

**`output_dir`**:  
Output directory for the corpus.

**`categories`**:  
Categories to get articles from.

**`props`**:  
Article properties to save to the corpus' json files.

**`title_filter`**:  
Specify substrings that article titles should not contain (.e.g. "Template:", "List of", ...)

**`revision_limit`**:  
Limit of revisions to get per API call.

**`category_member_limit`**:  
Limit of category members to get per API call.

**`min_article_length`**:  
Minimum article length in characters.

**`min_article_revisions`**:  
Minimum amount of article revisions.
