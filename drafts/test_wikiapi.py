#!/usr/bin/python3
"""
    This script uses the mediawiki API to get revisions.
    It doesn't need any extra dependencies and is a lot simpler than test_wikibot.py

    API doc: https://www.mediawiki.org/wiki/API:Revisions
    Example: https://en.wikipedia.org/w/api.php?action=query&prop=revisions&titles=Re-Volt&rvslots=*&rvprop=|user|timestamp|content&rvlimit=5
"""

import requests
import json

session = requests.Session()

url = "https://en.wikipedia.org/w/api.php"
titles = ["Re-Volt"]
props = ["timestamp", "user", "comment", "content"]

file_out = "output.json"

params = {
    "action": "query",
    "prop": "revisions",
    "titles": '|'.join(titles),
    "rvprop": '|'.join(props),
    "rvslots": "main",  # https://www.mediawiki.org/wiki/Manual:Slot
    "rvlimit": "100",
    "formatversion": "2",
    "format": "json"
}

r = session.get(url=url, params=params)
data = r.json()

with open(file_out, "w") as f:
    f.write(json.dumps(data, indent=4, sort_keys=False))
