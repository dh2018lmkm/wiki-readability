"""

	This doesn't make much sense. We can just directly use the API.
	See test_wikiapi.py


	---------------------------------------------


	Dependencies:
		- pywikibot (pip install pywikibot)

	If you don't want to use pywikibot with an account, set PYWIKIBOT_NO_USER_CONFIG=1:

	Either type `export PYWIKIBOT_NO_USER_CONFIG=1` into your terminal and log out and in
	or launch the script like this:
	`PYWIKIBOT_NO_USER_CONFIG=1 python test_wikibot.py`
"""

import pywikibot
# import dewiki

site = pywikibot.Site(u"en", fam=u"wikipedia")
wpage = pywikibot.Page(site, u"Hkun Law")

# entire edit history of page
history = wpage.revisions(total=1, content=True)

for page in history:
	# splits page into header, body (and separate sections) and footer
	page_sectioned = pywikibot.textlib.extract_sections(page.text)
	print(f"Header:\n{page_sectioned[0]}\n\n")
	for section in page_sectioned[1]:
		print(f"Section {section[0]}")
	print(f"\n\nFooter:\n{page_sectioned[2]}\n\n")


"""
return of pywikibot.textlib.extract_sections:

(header, body, footer)
header = "'''A''' is a thing."
body = [('== History of A ==', 'Some history...'),
        ('== Usage of A ==', 'Some usage...')]
footer = '[[Category:Things starting with A]]'
"""
