import os
import json


class Article:
    def __init__(self, path_article):

        with open(path_article, "r") as f:
            self.data = json.load(f)

    def get_revision_content(self, num):
        return self.data["query"]["pages"][0]["revisions"][num]["slots"]["main"]["content"]



art = Article("../src/captive_breeding.json")

print(art.get_revision_content(213))

with open("out.txt", "w") as f:
    f.write(art.get_revision_content(213))
