# wiki-readability

![image](docs/wikireadability_concept.png)

- `src/`: actual source code of the project
- `docs/`: contains code documentation
- `drafts/`: test scripts

## Dependencies

```
    sudo pip install pyphen  # for readability analysis
    sudo pip install matplotlib  # for static graphs
    sudo pip install bokeh  # for interactive visualization
```

You may have to use `pip3` instead.

## Usage

```bash
Usage:
python run.py <options>

-h or --help: Displays this help message.
-l or --build-list: Builds the article list
-b or --build-corpus: Builds the entire corpus
-c or --clean-corpus: Builds a new clean corpus
-a or --analyze: Analyzes the corpus for readability
-v or --visualize: Outputs graphs to folder
-f or --full: Does a complete run (all of the above except help)
```

You may have to use `python3` instead.

### Interactive Visualization

Generate data with `python run.py -f` and then run

```bash
bokeh serve interactive.py
```

Open [http://localhost:5006/interactive](http://localhost:5006/interactive) in your browser.


## Documentation

See code for now, every module is commented.

## References

Useful links and API references:

- Mediawiki API
	- [Category Members](https://www.mediawiki.org/wiki/API:Categorymembers)
    - [Revisions](https://www.mediawiki.org/wiki/API:Revisions)
