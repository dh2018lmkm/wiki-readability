#!/usr/bin/python3
import os
from sys import argv
from wikiread import config, analyze, corpus, visualize

def main():
    # option for displaying the help message
    if any(["-h" in argv, "--help" in argv]) or len(argv) == 1:
        print(
            "\nUsage:\n"
            "python run.py <options>\n\n"
            "-h or --help: Displays this help message.\n"
            "-l or --build-list: Builds the article list\n"
            "-b or --build-corpus: Builds the entire corpus\n"
            "-c or --clean-corpus: Builds a new clean corpus\n"
            "-a or --analyze: Analyzes the corpus for readability\n"
            "-v or --visualize: Outputs graphs to folder\n"
            "-f or --full: Does a complete run (all of the above except help)"
        )
        return

    # option to build the list of articles
    do_build_list = any(["-l" in argv, "--build-list" in argv])

    # option to build the corpus
    do_build_corpus = any(["-b" in argv, "--build-corpus" in argv])

    # option to create a new clean corpus
    do_clean_corpus = any(["-c" in argv, "--clean-corpus" in argv])

    # option to analyze the corpus
    do_analyze_corpus = any(["-a" in argv, "--analyze" in argv])

    # option to visualize
    do_visualize_corpus = any(["-v" in argv, "--visualize" in argv])

    # full
    if not any([do_build_list, do_build_corpus, do_clean_corpus, do_analyze_corpus, do_visualize_corpus]):
        do_build_list = do_build_corpus = do_clean_corpus = do_analyze_corpus = do_visualize_corpus = any(["-f" in argv, "--full" in argv])

    # builds the article list
    if do_build_list:
        corpus.build_list()

    # builds the complete corpus and saves it to files
    if do_build_corpus:
        corpus.build()

    if do_clean_corpus:
        corpus.clean()

    if do_analyze_corpus:
        analyze.generate_stats()

    if do_visualize_corpus:
        visualize.generate_graphs()

if __name__ == "__main__":
    main()
