import os
import json
import numpy as np

from bokeh.io import curdoc
from bokeh.palettes import Spectral4
from bokeh.layouts import row, column
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Slider, TextInput, MultiSelect, Div, Button
from bokeh.models.tools import CrosshairTool
from bokeh.plotting import figure

from wikiread import config
from wikiread.helpers import clamp, clean_filename

PLOT_WIDTH = config["interactive"]["plot_width"]
PLOT_HEIGHT = config["interactive"]["plot_height"]

with open(os.path.join(config["general"]["results_dir"], config["general"]["results_file"])) as f:
    results = json.load(f)

# Article list
art_options = []
art_options_dict = {}
# Easy access y-ranges for the graphs
art_lengths = {}
art_links = {}
art_sections = {}

for art in sorted(results["articles"]):
    if len(art) > 32:
        art_options.append(
            (clean_filename(art), art[:29] + "...")
        )
    else:
        art_options.append(
            (clean_filename(art), art)
        )
        
    art_options_dict[clean_filename(art)] = art
    art_lengths[art] = max([r["length"] for r in results["articles"][art]["revisions"]])
    art_links[art] = max([r["links_internal"] + r["links_external"] for r in results["articles"][art]["revisions"]]) 
    art_sections[art] = max([r["sections"] for r in results["articles"][art]["revisions"]])
print("Done setting up cache.")


class GraphApp:
    def __init__(self):
        # List to select an article from
        self.article_select = MultiSelect(title="Article:", options=art_options, size=32, height=700, value=[art_options[0][0]])

        # List of inputs on the left side
        self.inputs = column(Div(text=f"<h2>Wiki Readability</h2>"), self.article_select)

        # Button to compare/refresh
        self.button_refresh = Button(label="Compare", button_type="success", width=800)

        # Sliders to select two revisions to compare
        self.slider_rev1 = Slider(width=400, title="Revision 1", value=0, start=0, end=499, step=1)
        self.slider_rev2 = Slider(width=400, title="Revision 2", value=1, start=0, end=499, step=1)

        # Plots the article and shows it to the user
        self.plot_article(results["articles"][list(results["articles"].keys())[0]]["title"])
        self.setup_doc(results["articles"][list(results["articles"].keys())[0]]["title"])

    def setup_doc(self, title=""): 
        # Middle column
        column_plots = column(
                              Div(text=f"<h2>{title}</h2>", width=PLOT_WIDTH),                            
                              self.plot_flesch, 
                              self.plot_sections,
                              self.plot_length,
                              self.plot_links,
                       )

        # Right column with two texts
        column_texts = column(row(self.slider_rev1, self.slider_rev2), self.button_refresh, row(self.div1, self.div2), width=1000)

        # Adds all to the codument root
        curdoc().add_root(row(self.inputs, column_plots, column_texts))
        curdoc().title = f"{title} | Wiki Readability"

    def plot_article(self, title):
        plot_height = PLOT_HEIGHT

        # Article from results file
        article = results["articles"][title]

        # Article from corpus
        with open(os.path.join(config["corpus"]["output_dir"], "{}.json".format(clean_filename(title)))) as f:
            article_corpus = json.load(f)

        # Axes
        x = list(range(len(article["revisions"])))
        y1 = []
        y2 = []
        y3 = []
        y4 = []

        # Gets data from results
        for rev in article["revisions"]:
            y1.append(clamp(0.0, 100.0, rev["flesch_score"]))
            # if rev["flesch_score"] > 100.0:
                # plt.plot()
            y2.append(rev["sections"])
            y3.append(rev["links_internal"] + rev["links_external"])
            y4.append(rev["length"])

        self.source1 = ColumnDataSource(data=dict(x=x, y=y1))
        self.source2 = ColumnDataSource(data=dict(x=x, y=y2))
        self.source3 = ColumnDataSource(data=dict(x=x, y=y3))
        self.source4 = ColumnDataSource(data=dict(x=x, y=y4))

        # Revision selection from sliders
        rev1 = clamp(0, len(article["revisions"])-1, self.slider_rev1.value)
        rev2 = clamp(0, len(article["revisions"])-1, self.slider_rev2.value)

        # Sets up text divs
        self.div1 = Div(width=400, text="<h2>Revision {}</h2>".format(str(rev1)) + article_corpus["query"]["pages"][0]["revisions"][rev1]["slots"]["main"]["content_clean"])
        self.div2 = Div(width=400, text="<h2>Revision {}</h2>".format(str(rev2)) + article_corpus["query"]["pages"][0]["revisions"][rev2]["slots"]["main"]["content_clean"])
        
        # Sets up sliders to select revisions again
        self.slider_rev1 = Slider(width=400, title="Revision Left", value=rev1, start=0, end=len(article["revisions"])-1, step=1)
        self.slider_rev2 = Slider(width=400, title="Revision Right", value=rev2, start=0, end=len(article["revisions"])-1, step=1)

        # Creates figures for the graphs
        self.plot_flesch = figure(plot_height=plot_height, plot_width=PLOT_WIDTH, title="Flesch Score",
            tools="pan,reset,save,wheel_zoom, hover",
            x_range=[0, len(x)], y_range=[0, 100])
        self.plot_flesch.add_tools(CrosshairTool(dimensions="height"))
        self.plot_length = figure(plot_height=plot_height, plot_width=PLOT_WIDTH, title="Length",
            tools="pan,reset,save,wheel_zoom, hover",
            x_range=[0, len(article["revisions"])], y_range=[0, art_lengths[title]])
        self.plot_length.add_tools(CrosshairTool(dimensions="height"))
        self.plot_links = figure(plot_height=plot_height, plot_width=PLOT_WIDTH, title="Links",
            tools="pan,reset,save,wheel_zoom, hover",
            x_range=[0, len(article["revisions"])], y_range=[0, art_links[title]])
        self.plot_links.add_tools(CrosshairTool(dimensions="height"))
        self.plot_sections = figure(plot_height=plot_height, plot_width=PLOT_WIDTH, title="Sections",
            tools="pan,reset,save,wheel_zoom, hover",
            x_range=[0, len(article["revisions"])], y_range=[0, art_sections[title]])
        self.plot_sections.add_tools(CrosshairTool(dimensions="height"))

        # Puts data into figures
        self.plot_flesch.line('x', 'y', source=self.source1, line_width=3, line_alpha=0.9, color=Spectral4[0])
        self.plot_sections.line('x', 'y', source=self.source2, line_width=3, line_alpha=0.9, color=Spectral4[1])
        self.plot_links.line('x', 'y', source=self.source3, line_width=3, line_alpha=0.9, color=Spectral4[2])
        self.plot_length.line('x', 'y', source=self.source4, line_width=3, line_alpha=0.9, color=Spectral4[3])


# Creates the app and displays it
graphapp = GraphApp()

# Sets up callbacks
def update_title(attrname, old, new):
    title = art_options_dict[graphapp.article_select.value[0]]
    graphapp.plot_article(title)
    curdoc().clear()
    graphapp.setup_doc(title)

def update_title2():
    title = art_options_dict[graphapp.article_select.value[0]]
    graphapp.plot_article(title)
    curdoc().clear()
    graphapp.setup_doc(title)

# Reloads article if selection changed
graphapp.article_select.on_change('value', update_title)
graphapp.button_refresh.on_click(update_title2)
