import os
import json
from wikiread import config, readability, inspector


def generate_stats():
    """
        Generates statistics and writes the results to a json file
        returns: None
    """
    corpus_dir = config["corpus"]["output_dir"]

    # dictionary for the results
    results = {
        "articles" : {}
    }

    # iterates through cleaned corpus to find out the flesch score
    num_articles = len(os.listdir(corpus_dir))
    count = 1
    for a in os.listdir(corpus_dir):
        with open(os.path.join(corpus_dir, a), "r") as f:
            article = inspector.Article(json.load(f))

        print(f"Generating statistics: {article.title} ({count} of {num_articles})...")

        num_revs = len(article.revisions)
        # creates a result entry for the article
        result = {
            "title": article.title,
            "revisions": []
        }
        results["articles"][article.title] = result

        # iterates through all revisions and finds out the flesch score
        for i in range(num_revs):
            content = article.get_revision_content(i)
            content_clean = article.get_revision_content_clean(i)
            num_syllables = readability.count_syllables(content_clean)
            num_words = readability.count_words(content_clean)
            num_sentences = readability.count_sentences(content_clean)
            timestamp = article.revisions[i]["timestamp"]
            
            flesch_score = readability.get_flesch(num_words, num_sentences, num_syllables)
            print(f"Revision {i+1} of {num_revs}: {flesch_score}", end='\r')
            links_internal = content.count("http://") if content else 0
            links_external = links_internal + content.count("https://") if content else 0

            result["revisions"].append(
                {
                    "timestamp": timestamp,
                    "flesch_score": flesch_score,
                    "sections": content.count("\n==") if content else 0,
                    "links_internal": links_internal,
                    "links_external": links_external,
                    "linked_files": content.count("[[File") if content else 0,
                    "length": len(content),
                }
            )
        count += 1
    print('\n')

    # writes results to a file
    with open(os.path.join(config["general"]["results_dir"], config["general"]["results_file"]), "w") as f:
        f.write(json.dumps(results, indent=4, sort_keys=False))

