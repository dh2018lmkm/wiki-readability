"""
    Module for handling json files with mediawiki query responses as objects
"""

class Article:
    """
        Class for handling articles in the mediawiki query response json files
    """
    def __init__(self, data):
        self.data = data

    @property
    def title(self):
        """
            Title of the article
        """
        return self.data["query"]["pages"][0]["title"]

    @property
    def revisions(self):
        """
            List of revisions
        """
        return self.data["query"]["pages"][0]["revisions"]

    def get_revision_content(self, num_revision):
        """
            Gets the content of a certain revision
            parameters:
                num_revision: int, index of the revision to get
            returns: string, content of the revision
        """
        slot_main = self.data["query"]["pages"][0]["revisions"][num_revision]["slots"]["main"]
        if "content" in slot_main:
            return slot_main["content"]
        else:
            return ""

    def get_revision_content_clean(self, num_revision):
        """
            Gets the clean content of a certain revision
            parameters:
                num_revision: int, index of the revision to get
            returns: string, content of the revision
        """
        slot_main = self.data["query"]["pages"][0]["revisions"][num_revision]["slots"]["main"]
        if "content_clean" in slot_main:
            return slot_main["content_clean"]
        else:
            return ""
