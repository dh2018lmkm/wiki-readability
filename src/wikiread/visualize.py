import os
import json
import numpy as np
import matplotlib.pyplot as plt

from bokeh.layouts import gridplot
from bokeh.models import ColumnDataSource, CDSView, IndexFilter
from bokeh.plotting import figure, show

from wikiread import config, helpers
from wikiread.helpers import clamp


def generate_graphs():
    """
        Generates graph PNGs
        returns: nothing
    """
    with open(os.path.join(config["general"]["results_dir"], config["general"]["results_file"]), "r") as f:
        results = json.load(f)

    for a in results["articles"]:
        article = results["articles"][a]
        title = article["title"]
        print(f"Creating graph for {title}...")
        x = list(range(len(article["revisions"])))
        y1 = []
        y2 = []
        y3 = []
        for rev in article["revisions"]:
            y1.append(clamp(0.0, 100.0, rev["flesch_score"]))
            # if rev["flesch_score"] > 100.0:
                # plt.plot()
            y2.append(rev["sections"])
            y3.append(rev["links_internal"] + rev["links_external"])

        plt_readability = plt.plot(x, y1, label="readability")
        plt_sections = plt.plot(x, y2, label="sections")
        plt_links = plt.plot(x, y3, label="links")

        # plt.legend()
        plt.legend()
        plt.xlabel('Revision')
        plt.ylabel('Amount')
        plt.title(article["title"])
        plt.grid(True)
        plt.savefig(os.path.join(
            config["general"]["results_dir"],
            config["general"]["results_graphs_dir"],
            helpers.clean_filename(f"{title}.png")
            )
        )
        # plt.show()
        plt.clf()
        plt.cla()
    print("Done visualizing. Check {}".format(os.path.join(
            config["general"]["results_dir"],
            config["general"]["results_graphs_dir"])))

