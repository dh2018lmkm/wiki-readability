import sys      # for using sys.argv
import re       # for performing a regex search
import os       # to check for the existance of files


# parses the JSON file for all articles and returns them as a list
def get_all_entries(path):
    with open(path, "r") as f:      # get all data from JSON file
        data = f.read()

    entries = []                    # list to save all the entries in
    key = "\"content\": \""         # key to search for

    while True:                     # get all entries themselves, as long as the key is found
        pos1 = data.find(key)       # find key's position

        if pos1 == -1:              # if not found, stop
            break

        data = data[pos1+len(key):]                         # delete the things before the key
        m = re.search("[\"](\ )*(\n)*(\ )*[}]", data, 0)    # find the end of the content, marked by ", spaces, new line, spaces and }
        pos2 = m.start()                                    # position where end occurs
        entries.append(data[:pos2])                         # add the entry to the entries list

    return entries


# entries is the list to be changed
# starts contains a list of characters that indicate the beginning
# ends contains an equally sized list of chars that indicate the end
# removes everything between starts and ends inclusively
def remove_between(entries, starts, ends):
    for j in range(len(entries)):                           # go through each entry
        for i in range(len(starts)):                        # go through each block that should be removed
            while True:
                starttag = entries[j].find(starts[i])           # find start of block
                if starttag == -1:                              # break should none be left to remove
                    break
                endtag = entries[j].find(ends[i], starttag)+len(ends[i])            # find end of block and account for the end-character(s)
                entries[j] = entries[j].replace(entries[j][starttag:endtag], " ")   # replace the block with nothing

    return entries


# makes links readable without deleting the actual words
def correct_links(entries):
    for i in range(len(entries)):                   # for all entries
        while True:                                 # as long as links are found
            starttag = entries[i].find("[[")        # look for beginning of link
            if starttag == -1:                      # break if none left
                break
            endtag = entries[i].find("]]", starttag)                        # end of link
            linkcontent = entries[i][starttag:endtag+2]                     # the link itself
            
            if entries[i].find("|", starttag, endtag+2) != -1:           
                splitterpos = entries[i].find("|", starttag, endtag+2)          # the position of the | within the link from [[something|anything]]
            elif entries[i].find(":", starttag, endtag+2) != -1:
                splitterpos = entries[i].find(":", starttag, endtag+2)          # the position of the : within the link from [[de:name]]
            elif entries[i].find("#", starttag, endtag+2) != -1:
                splitterpos = entries[i].find("#", starttag, endtag+2)          # the position of the # within the link
            else:
                splitterpos = -1
                
            if splitterpos != -1:                                                               # if there is one
                entries[i] = entries[i].replace(entries[i][starttag:splitterpos+1], " ")        # remove everything before it, inclusively
            else:
                entries[i] = entries[i].replace(entries[i][starttag:starttag+2], " ")           # remove the start tags
            entries[i] = entries[i].replace(entries[i][endtag:endtag+2], " ")               # remove the ending ]]

    return entries


# removes the wikitext formatting to return plaintext
def get_plaintext(entries):
    starts = ["<--", "<", "{{", "=", "'"]               # characters at start of block that should be removed
    ends = ["-->", ">", "}}", "=", "'"]                 # characters at end -||-
    entries = remove_between(entries, starts, ends)     # remove everything in between
    entries = correct_links(entries)                    # delete URL and formatting and only leave the word(s) themselves

    with open("chars_to_remove.csv", "r") as f:         # get characters to delete from given csv file
        remove_list = f.read().split(",")               # convert CSV data to array

    for i in range(len(entries)):                         # remove all occurrences in all files
        for item in remove_list:
            entries[i] = entries[i].replace(item, " ")

        while entries[i].find("  ") != -1:                # correct all double spaces as long as there are any
            entries[i] = entries[i].replace("  ", " ")

        if entries[i][0] == " ":                # remove leading whitespace
            entries[i] = entries[i][1:]

    return entries


# returns the path(s) of the JSON file(s) that should be used
def get_paths():
    if len(sys.argv) < 2:
        return input("Path: ")      # manually if none specified through terminal
    else:
        return sys.argv[1:]         # otherwise use all from terminal


# puts each entry into one file in given directory
def dump_to_file(path, entries):
    path = path.split(".")[0] + ".txt"                      # rename it from <file>.json to <file>.txt
    with open(path, "w") as f:
        for i in range(len(entries)):                       # write all entries into a file
            f.write(entries[i])
            f.write("\n\n#########################\n\n")    # arbitrary seperator


def main():
    paths = get_paths()                         # get all the given paths
    for i in range(len(paths)):
        if not os.path.exists(paths[i]):                            # check if the file even exists
            print("Error! " + str(paths[i]) + " doesn't exist!")    # error message
            continue                                                # jump to next file

        entries = get_all_entries(paths[i])     # parse JSON file to get the entries themselves
        entries = get_plaintext(entries)        # convert to plaintext
        dump_to_file(paths[i], entries)         # puts it back into files

    return 0

if __name__ == "__main__":
    main()
