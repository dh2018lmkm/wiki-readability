import os
import json
import logging

__all__ = ["wapi", "corpus", "helpers", "analyze", "readability", "to_plaintext"]

# Resets logging level to warnings only
logging.getLogger("urllib3").setLevel(logging.WARNING)

# Opens config files
with open('config.json') as config_file:
    config = json.load(config_file)

# Creates corpus directory if there is none
if not os.path.isdir(config["corpus"]["output_dir"]):
    os.mkdir(config["corpus"]["output_dir"])

# Creates results dir if there is none
if not os.path.isdir(config["general"]["results_dir"]):
    os.mkdir(config["general"]["results_dir"])
if not os.path.isdir(os.path.join(config["general"]["results_dir"], config["general"]["results_graphs_dir"])):
    os.mkdir(os.path.join(config["general"]["results_dir"], config["general"]["results_graphs_dir"]))

# Opens cache
if not os.path.isfile(config["general"]["cache_file"]):
    with open(config["general"]["cache_file"], "w") as f:
        f.write(json.dumps({}))

with open(config["general"]["cache_file"]) as f:
    cache = json.load(f)


def write_cache():
    """
        Writes the cache back to the file
    """
    with open(config["general"]["cache_file"], "w") as f:
        f.write(json.dumps(cache))
