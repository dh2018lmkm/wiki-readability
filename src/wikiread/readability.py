import pyphen
from wikiread import config


def count_syllables(text):
    """
        Counts syllables of a text.
        parameters:
            text: string containing the text
        returns: int of sillables
    """
    dic = pyphen.Pyphen(lang='en')
    text = text.split(' ')
    syllables = 0
    for word in text:
        syllables += len(dic.positions(word)) + 1
    return syllables


def count_words(text):
    """
        Counts the words in a text.
        parameters:
            text: string containing the text
    """
    return len(text.split(" "))


def count_sentences(text):
    """
        Counts the sentences in a text.
        parameters:
            text: string containing the text
    """
    return len(text.split('.'))


def get_flesch(num_words, num_sentences, num_syllables):
    """
        Calculates the Flesch-Kincaid reading ease score
        parameters:
            num_words: int, total words of the text
            num_sentences: int, total number of sentences
            num_syllables: int, total number of syllables
        returns: reading ease score from 0.0 to 100.0
    """
    if any([num_words == 0, num_words == 0]):
        return 0
    return 206.835 - 1.015 * (num_words / num_sentences) - 84.6 * (num_syllables / num_words)
