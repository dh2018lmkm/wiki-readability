import re

def clean_filename(title):
    """
        Replaces unsupported characters for clean file names
        parameters:
            title: any string
        returns: clean string
    """
    title = re.sub('[^\w\s-]', '', title).strip().lower()
    title = re.sub('[-\s]+', '_', title)
    return title


def clamp(min, max, value):
    if value < min:
        return min
    if value > max:
        return max
    return value
