import sys
import requests
from wikiread import config, cache, write_cache
from time import sleep


def call(params):
    """
        Performs an API call.
        parameters:
            params: dict with parameters following the Mediawiki API specs
        returns:
            dict with the response
    """
    sleep(config["general"]["sleep"])
    session = requests.Session()
    url = config["corpus"]["wiki_url"]
    request = session.get(url=url, params=params)
    data = request.json()

    if "continue" in data:
        print('█', end='')
        sys.stdout.flush()
        if "cmcontinue" in data["continue"]:
            params["cmcontinue"] = data["continue"]["cmcontinue"]
        elif "rvcontinue" in data["continue"]:
            params["rvcontinue"] = data["continue"]["rvcontinue"]
        more = call(params)
        if "pages" in data["query"]:
            # assumes page 0 since we're only querying one page at a time
            data["query"]["pages"][0]["revisions"] += more["query"]["pages"][0]["revisions"]
        del data["continue"]
        print("█", end='\r')
        sys.stdout.flush()
    return data


def get_subcats(categories):
    """
        Gets immediate sub-categories from the categories set in config.json.
        parameters:
            categories: list of category titles
        returns: list of category names
    """
    sub_categories = []

    count = 1
    length = len(categories)
    for cat in categories:
        print(f"Getting sub-categories for {cat} ({count} of {length})")
        params = {
            "action": "query",
            "cmlimit": str(config["corpus"]["category_member_limit"]),
            "cmtitle": cat,
            "cmtype": "subcat",
            "format": "json",
            "list": "categorymembers"
        }
        data = call(params)

        for mem in data["query"]["categorymembers"]:
            sub_categories.append(mem["title"])
        count += 1
    return list(set(sub_categories))


def get_article_list(categories):
    """
        Gets a list of article titles from the categories specified.
        parameters:
            categories: list of category titles
        returns: list of strings with article names
    """
    articles = []
    count = 1
    for cat in categories:
        length = len(categories)
        print(f"Getting all titles from {cat} ({count} of {length})")
        params = {
            "action": "query",
            "cmlimit": str(config["corpus"]["category_member_limit"]),
            "format": "json",
            "list": "categorymembers",
            "cmtype": "page",
            "cmtitle": cat
        }
        data = call(params)

        for page in data["query"]["categorymembers"]:
            title = page["title"]
            if not any([t in title for t in config["corpus"]["title_filter"]]):
                articles.append(title)
        count += 1
    return list(set(articles))


def get_page_revisions(title):
    """
        Gets all revisions of a page.
        parameters:
            title: string, title of the article
        returns: dict with page data
    """
    params = {
        "action": "query",
        "prop": "revisions",
        "titles": title,
        "rvprop": '|'.join(config["corpus"]["props"]),
        "rvslots": "main",  # https://www.mediawiki.org/wiki/Manual:Slot
        "rvlimit": str(config["corpus"]["revision_limit"]),
        "formatversion": "2",
        "format": "json"
    }
    data = call(params)
    return data


def get_page_revision_num(title, cached=True):
    """
        Counts all revisions of a page.
        parameters:
            title: string, title of the article
            cached: bool, whether to use value from cache
        returns: int, amount of revisions
    """

    if not "page_revision_num" in cache:
        cache["page_revision_num"] = {}

    if cached and title in cache["page_revision_num"]:
        return cache["page_revision_num"][title]

    params = {
        "action": "query",
        "prop": "revisions",
        "titles": title,
        "rvprop": "",
        "rvslots": "main",  # https://www.mediawiki.org/wiki/Manual:Slot
        "rvlimit": str(config["corpus"]["revision_limit"]),
        "formatversion": "2",
        "format": "json"
    }
    data = call(params)
    num_revisions = len(data["query"]["pages"][0]["revisions"])
    cache["page_revision_num"][title] = num_revisions
    write_cache()
    return num_revisions


def get_page_length(title, cached=True):
    """
        Returns the lenght of a page.
        parameters:
            title: string, title of the article
            cached: bool, whether to use value from cache
        returns: int, lenght of article in chars
    """
    if not "page_length" in cache:
        cache["page_length"] = {}

    if cached and title in cache["page_length"]:
        return cache["page_length"][title]

    params = {
        "action": "query",
        "prop": "info",
        "titles": title,
        "formatversion": "2",
        "format": "json"
    }
    data = call(params)
    page_length = data["query"]["pages"][0]["length"]
    cache["page_length"][title] = page_length
    write_cache()
    return page_length
