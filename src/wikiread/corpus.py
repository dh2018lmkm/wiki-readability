import os
import sys
import json
import time
import unwiki
from wikiread import config, inspector, wapi, helpers, to_plaintext


def build_list():
    """
        Builds the list of articles for the corpus and writes it to a file
        returns: None
    """
    corpus_list = {"articles": []}
    corpus_list_file = config["corpus"]["corpus_list_file"]
    corpus_dir = config["corpus"]["output_dir"]

    # list of categories and subcategories
    categories = config["corpus"]["categories"] +  wapi.get_subcats(config["corpus"]["categories"])

    print("Getting articles from the following categories:")
    [print(f"    {c}") for c in categories]

    article_list = wapi.get_article_list(categories)

    num_arts = len(article_list)
    count = 1
    for title in article_list:
        print(f"Checking requirements for {title} ({count} of {num_arts})")
        length = wapi.get_page_length(title)
        num_revs = wapi.get_page_revision_num(title)

        if length >= config["corpus"]["min_article_length"] and num_revs >= config["corpus"]["min_article_revisions"]:
            corpus_list["articles"].append(title)
        count += 1

    # gets all titles of articles in categories
    # writes the corpus article list to a file
    with open(corpus_list_file, "w") as f:
        f.write(json.dumps(corpus_list, indent=4, sort_keys=False))


def build():
    """
        Builds the corpus
        returns: None
    """
    print("Starting to build the corpus...")
    time_start = time.time()

    corpus_list_file = config["corpus"]["corpus_list_file"]
    corpus_list = {}

    # loads existing corpus article list
    with open(corpus_list_file, "r") as f:
        corpus_list["articles"] = json.load(f)["articles"]

    length = len(corpus_list["articles"])
    count = 1
    for title in corpus_list["articles"]:
        print(f"Getting article {title} ({count} of {length})")
        out_file = os.path.join(config["corpus"]["output_dir"], f"{helpers.clean_filename(title)}.json")
        if os.path.isfile(out_file):
            print("Already in corpus, skipping...")
            count += 1
            continue
        data = wapi.get_page_revisions(title)  # downloads the article

        # Writes the file of the article
        file_out = os.path.join(out_file)
        with open(file_out, "w") as f:
            f.write(json.dumps(data, indent=4, sort_keys=False))

        count += 1

    time_end = time.time()

    print(f"Finished building the corpus in {time_end - time_start} seconds.")


def clean():
    """
        Cleans the corpus
        returns: None
    """

    corpus_dir = config["corpus"]["output_dir"]

    num_articles = len(os.listdir(corpus_dir))
    count = 1
    for a in os.listdir(corpus_dir):
        with open(os.path.join(corpus_dir, a), "r") as f:
            article = json.load(f)
            iarticle = inspector.Article(article)

        title = article["query"]["pages"][0]["title"]
        print(f"Cleaning {title} ({count} of {num_articles})...")

        num_revs = len(article["query"]["pages"][0]["revisions"])
        delete = []
        for i in range(num_revs):
            article["query"]["pages"][0]["revisions"][i]["slots"]["main"]["content_clean"] = "FAILED"
            try:
                article["query"]["pages"][0]["revisions"][i]["slots"]["main"]["content_clean"] = unwiki.loads(article["query"]["pages"][0]["revisions"][i]["slots"]["main"]["content"])
                # article["query"]["pages"][0]["revisions"][i]["slots"]["main"]["content_clean"] = to_plaintext.get_plaintext([article["query"]["pages"][0]["revisions"][i]["slots"]["main"]["content"]])[0]
            except Exception as e:
                print(f"Error: Could not clean revision {i}:\n{e}")
                pass
            print(f"Revision {i+1} of {num_revs}", end='\r')
        
        print("Cleaning empty revisions...")
        for rev in article["query"]["pages"][0]["revisions"]:
            if "content" not in rev["slots"]["main"] or any([
                rev["slots"]["main"]["content"] == "",
                rev["slots"]["main"]["content_clean"] == "FAILED",
                rev["slots"]["main"]["content_clean"] == ""
                ]):
                article["query"]["pages"][0]["revisions"].remove(rev)

        file_out = os.path.join(corpus_dir, a)

        with open(file_out, "w") as f:
            f.write(json.dumps(article, indent=4, sort_keys=False))
        count += 1
    print('\n')


